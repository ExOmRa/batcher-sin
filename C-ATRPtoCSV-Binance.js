const config = {}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                          GENERAL SETTINGS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Name of batch (will show up in generated csv's file name )
config.name = 'C-ATRPtoCSV-Binance'

// Save results to csv
config.saveToCsv = true

config.gekkoPath = '../gekko-sin/'
config.gekkoConfigFileName = 'config.js'

// URL that serving Gekko UI
config.apiUrl = 'http://localhost:3000'

// Keep it lower than the number of cores you have
// Note: 2 is recommended value for import mode
config.parallelQueries = 2

config.candleSizes = [1, 5]

config.historySizes = [300]

// Format: [exchange, currency, asset]
config.tradingPairs = [
	['binance', 'btc', 'matic'],
	['binance', 'btc', 'bnb'],	
	['binance', 'btc', 'link'],	
	['binance', 'btc', 'one'],	
	['binance', 'btc', 'erd'],	
	['binance', 'btc', 'ltc'],	
	['binance', 'btc', 'xrp'],
	['binance', 'btc', 'eos'],
	['binance', 'btc', 'ren'],
	['binance', 'btc', 'bchabc'],
	['binance', 'btc', 'celr'],
	['binance', 'btc', 'algo'],
	['binance', 'btc', 'ada'],
	['binance', 'btc', 'qkc'],
	['binance', 'btc', 'xmr'],
	['binance', 'btc', 'trx'],
	['binance', 'btc', 'vet'],
	['binance', 'btc', 'fet'],
	['binance', 'btc', 'ftm'],
	['binance', 'btc', 'zec'],
	['binance', 'btc', 'neo'],
	['binance', 'btc', 'bat'],
	['binance', 'btc', 'waves'],
	['binance', 'btc', 'rvn']

]

// Note: only one daterange for "import" mode allowed if parallelQueries > 1
// It's related to constraints with database
config.dateranges = [{
  from: '2019-07-07 00:00',
  to: '2019-09-07 00:00'
}]

// Shuffle generated combinations of method's configs
config.shuffle = true

// Initial balance, fees and slippage/spread
config.paperTrader = {
  simulationBalance: {
    currency: 100,
    asset: 1
  },
  feeMaker: 0.075,
  feeTaker: 0.075,
  feeUsing: 'maker',
  slippage: 0.05
}

// Where to get method's settings.
// The first has high priority. Then second, if there's no settings in first place and so on.

// batcher – strategy settings below here
// gekko – gekko's config.js
// toml.js – gekko's toml files
config.configPriorityLocations = ['batcher', 'gekko', 'gekko-toml']

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                          BACKTEST BATCHER
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

config.methods = ['ATRPtoCSV']

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                          BRUTEFORCE SEARCHER
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Specify strategy you want for bruteforce
config.method = 'ATRPtoCSV'

// Specify ranges settings for the given method. It generates all
// possible combinations of a set of settings with given ranges
// Format for range: 'start:step:end' or 'true|false'
config.ranges = {
  interval: '8:1:10',
  thresholds: {
    low: '24:1:26',
    high: '70:1:80',
    persistence: 1
  }
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                          STRATEGY SETTINGS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

config.RSI = {
  interval: 15,
  thresholds: {
    low: 25,
    high: 75,
    persistence: 1
  }
}

config.MACD = {
  short: 10,
  long: 21,
  signal: 9,
  thresholds: {
    down: -0.000025,
    up: 0.000025,
    persistence: 1
  }
}

module.exports = config
